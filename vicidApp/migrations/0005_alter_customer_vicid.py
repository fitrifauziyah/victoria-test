# Generated by Django 4.1.11 on 2023-10-02 04:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vicidApp', '0004_customer_vicid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='VICID',
            field=models.CharField(editable=False, max_length=8, unique=True),
        ),
    ]
