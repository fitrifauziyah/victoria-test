# Generated by Django 4.1.11 on 2023-10-02 03:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vicidApp', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='VICID',
            field=models.CharField(max_length=8, unique=True),
        ),
    ]
