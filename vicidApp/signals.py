import random
from django.db.models.signals import pre_save
from django.dispatch import receiver
from .models import Customer

@receiver(pre_save, sender=Customer)
def generate_unique_VICID(sender, instance, **kwargs):
    if not instance.VICID:
        while True:
            VICID_value = str(random.randint(10000000, 99999999))
            if not Customer.objects.filter(VICID=VICID_value).exists():
                instance.VICID = VICID_value
                break
