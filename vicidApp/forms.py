from django import forms
from .models import Customer

class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = ['ACCTNO', 'JENIS_IDENTITAS', 'NO_IDEN', 'NAMA_NASABAH', 'NO_HP', 'JENIS_NASABAH', 'EMAIL']

    def __init__(self, *args, **kwargs):
        super(CustomerForm, self).__init__(*args, **kwargs)
        
        for field_name, field in self.fields.items():
            field.widget.attrs.update({'class': 'form-control'})