from django.utils import timezone
from django.db.models import OuterRef, Q, OuterRef, Exists
from django.shortcuts import render, redirect, get_object_or_404
from .forms import *
from .models import *

def customer_list(request):
    subquery = History.objects.filter(
        customer=OuterRef('pk'),
        APPRV_UPDT='In Approval'
    ).values('customer')

    customers = Customer.objects.filter(Q(is_active=True)).annotate(
        in_approval=Exists(subquery)
    ).all()

    return render(request, 'customer.html', {'customers': customers})

def add_customer(request):
    if request.method == 'POST':
        form = CustomerForm(request.POST)
        if form.is_valid():
            customer = form.save()
            return redirect('customer_list')
    else:
        form = CustomerForm()
    
    return render(request, 'add_customer.html', {'form': form})

def update_customer(request, customer_id):
    customer = get_object_or_404(Customer, customer_id=customer_id)
    if request.method == 'POST':
        form = CustomerForm(request.POST, instance=customer)
        if form.is_valid():
            customer_snapshot = Customer.objects.get(pk=customer_id)
            updated_customer = form.save()
            fields_changed = {}
            for field_name in form.changed_data:
                fields_changed[field_name] = getattr(customer_snapshot, field_name)

            for field_name, field_value in fields_changed.items():
                history_entry = History.objects.create(
                    customer=updated_customer,
                    Nama_Field=field_name,
                    Field_Awal=field_value,
                    Field_Update=getattr(updated_customer, field_name),
                    TGL_UPDT=timezone.now().strftime('%Y%m%d'),
                    JM_UPDT=timezone.now().strftime('%H%M%S'),
                    # USR_UPDT=request.user.username if request.user.is_authenticated else 'Anonymous',
                    APPRV_UPDT='In Approval'
                )
                history_entry.save()

            return redirect('customer_list')
    else:
        form = CustomerForm(instance=customer)
    
    return render(request, 'update_customer.html', {'form': form})

def delete_customer(request, customer_id):
    customer = get_object_or_404(Customer, customer_id=customer_id)
    customer_snapshot = Customer.objects.get(pk=customer_id)
    customer.is_active = False
    customer.save()

    history_entry = History.objects.create(
        customer=customer,
        Nama_Field='is_active',
        Field_Awal='True',
        Field_Update='False',
        TGL_UPDT=timezone.now().strftime('%Y%m%d'),
        JM_UPDT=timezone.now().strftime('%H%M%S'),
        JM_DELET=timezone.now().strftime('%Y%m%d'),
        TGL_DELET=timezone.now().strftime('%H%M%S'),
        # USR_DELET=request.user.username if request.user.is_authenticated else 'Anonymous',
        APPRV_DELET='In Approval'
    )
    history_entry.save()

    return redirect('customer_list')

def history_list(request):
    history_entries = History.objects.all()
    return render(request, 'history.html', {'history_entries': history_entries})
