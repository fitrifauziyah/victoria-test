from django.db import models

class User(models.Model):
    user_id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=50, unique=True)
    email = models.CharField(max_length=50, unique=True)
    phone_numer = models.CharField(max_length=17)
    password = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    profile_photo = models.ImageField(upload_to='images/')
    is_active = models.BooleanField

    def __str__(self):
        return self.user_id


class Customer(models.Model):
    customer_id = models.AutoField(primary_key=True)
    VICID = models.CharField(max_length=8, unique=True, editable=False)
    ACCTNO = models.CharField(max_length=10)
    JENIS_IDENTITAS = models.CharField(max_length=10, choices=[('KTP', 'KTP'), ('PASSPORT', 'Passport')])
    NO_IDEN = models.CharField(max_length=16)
    NAMA_NASABAH = models.CharField(max_length=50)
    NO_HP = models.CharField(max_length=17)
    JENIS_NASABAH = models.CharField(max_length=10, choices=[('Perusahaan', 'Perusahaan'), ('Individu', 'Individu')])
    EMAIL = models.EmailField(max_length=50)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.VICID  


class History(models.Model):
    history_id = models.AutoField(primary_key=True)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    Nama_Field = models.CharField(max_length=15)
    Field_Awal = models.CharField(max_length=50)
    Field_Update = models.CharField(max_length=50)
    Record_DEL = models.CharField(max_length=1)
    TGL_UPDT = models.CharField(max_length=8)
    JM_UPDT = models.CharField(max_length=8)
    TGL_APPRV_UPDT = models.CharField(max_length=8)
    JM_APPRV_UPDT = models.CharField(max_length=8)
    USR_UPDT = models.CharField(max_length=15)
    APPRV_UPDT = models.CharField(max_length=15)
    TGL_DELET = models.CharField(max_length=8)
    JM_DELET = models.CharField(max_length=8)
    TGL_APPRV_DELET = models.CharField(max_length=8)
    JM_APPRV_DELET = models.CharField(max_length=8)
    USR_DELET = models.CharField(max_length=15)
    APPRV_DELET = models.CharField(max_length=15)

    def __str__(self):
        return self.history_id